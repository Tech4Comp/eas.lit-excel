

function updateWorkbook(data, createNew) {

	console.log("updateWorkbook2");
	console.log(data);
	console.log(createNew);

	for (var name in data) {
		createSheet(name, data[name]);
	}

}




// async function createSheet (context, progress, name, sheetData) {

function createSheet(name, sheetData) {

	Excel.run(function (context) {

		/* delete + create sheet */
		var sheet = context.workbook.worksheets.getItemOrNullObject(name);
		// if (!(sheet.isNullObject)) {
		console.log(sheet);
		console.log("Delete sheet " + name);
		console.log(sheet.delete());
		console.log("Deleted");
		// }
		console.log("Add sheet " + name);

		sheet = context.workbook.worksheets.add(name);
		console.log(sheet);

		/* create header */
		console.log("Create header");

		const expensesTable = sheet.tables.add(sheet.getRangeByIndexes(0, 0, 1, sheetData[0].length), true);
		expensesTable.getHeaderRowRange().values = [sheetData[0]];
		// await context.sync();

		/* add data by chunks */
		var rowCount = sheetData.length;
		var dataArr = [];
		var i;
		const chunkSize = 10;
		for (i = 1; i < rowCount; i++) {
			dataArr.push(sheetData[i]);
			if (((i % chunkSize) == 0) || (i == (rowCount - 1))) {
				// progress.value += dataArr.length; 
				expensesTable.rows.add(null, dataArr);
				dataArr = [];
				// await context.sync();
			}
		}

		/* finishing */
		expensesTable.getDataBodyRange().format.autofitColumns();
		expensesTable.getDataBodyRange().format.autofitRows();
		sheet.visibility = "Visible";
		sheet.activate();
		// await context.sync();



		return context.sync();
	});




	// 	.then(function () {
	// 		console.log("Finished!");
	// 	  });
	// }).catch (function (error) {
	// 	console.log("Error");
	// 	console.log(error);
	// });




}









function storeExcelDataAsJSONCSV() {

	console.log ("START STORE");
	window.localStorage.removeItem("easlit-data");

	Excel.run(async function (context) {

		var sheets = context.workbook.worksheets;
		sheets.load("items/name");
		await context.sync();

		var data = new Object();
		data["All"] = await getSheetData(context, sheets, "All");

		// if we have a sheet named "All" --> this is the data; otherwise collect all sheets per item type
		if (data["All"] == null) {

			delete data["All"];
			const sheetNames = ['MC', 'SC', 'FT', 'OR'];
			for (var i = 0; i < sheetNames.length; i++) {
				data[sheetNames[i]] = await getSheetData(context, sheets, sheetNames[i]);
				if (data[sheetNames[i]] == null) {
					delete data[sheetNames[i]];
				}
			}
		}
		window.localStorage.setItem("easlit-data", JSON.stringify(data));
		console.log ("STORED");
		// console.log (JSON.stringify(data));
		return context.sync();
	});
}



async function getSheetData(context, sheets, sheetName) {

	for (var i in sheets.items) {

		var sheet = sheets.items[i];
		sheet.load("tables/items");
		await context.sync();

		//		console.log ("Sheet " + sheet.name + " has " + sheet.tables.items.length + " tables");

		if (sheet.name != sheetName) continue;
		if (sheet.tables.items.length == 0) continue;

		var table = sheet.tables.getItemAt(0);
		var headerRange = table.getHeaderRowRange().load("values");
		var bodyRange = table.getDataBodyRange().load("values");
		await sheet.context.sync();

		return headerRange.values.concat(bodyRange.values);
	}

	return null;
}



