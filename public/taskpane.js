//The initialize function must be run each time a new page is loaded
(function() {
	Office.initialize = function(reason) {
		$(document).ready(function() {
			$("#importForm").css('visibility', 'visible');
			$("#importForm").find("button").click(function(event) {
				var inputFormat = $("#importForm").find("select[name='inputFormat']").children("option:selected").val();
				var outputFormat = "csvjson_type";
				var file = $("#importForm").find("input[name='file']")[0].files[0];
				var progress = $("#importForm").find("progress")[0];
				sendPOST_Import (inputFormat, outputFormat, file, progress);
			});
			
			$("#downloadForm").css('visibility', 'visible');
			$("#downloadForm").find("button").click(function(event) {
				event.preventDefault();
				getJSONCSVData ($("#downloadForm"), $("#downloadForm").find("input[name='fileData']"));
				
			});
			
		});
	};
})();




async function getSheetData (context, sheets, sheetName) {
	
	for (var i in sheets.items) {
		
		var sheet = sheets.items[i];
		sheet.load ("tables/items");
		await context.sync();
		
//		console.log ("Sheet " + sheet.name + " has " + sheet.tables.items.length + " tables");
		
		if (sheet.name != sheetName) continue;
		if (sheet.tables.items.length == 0) continue;
		
		var	table = sheet.tables.getItemAt(0);
		var headerRange = table.getHeaderRowRange().load("values");
		var bodyRange = table.getDataBodyRange().load("values");
		await sheet.context.sync();
				
		return headerRange.values.concat (bodyRange.values); 
	}	
	
	return null;
}



function getJSONCSVData (downloadForm, fileData) {
	
	Excel.run(async function (context) { 

		var sheets = context.workbook.worksheets;
    	sheets.load("items/name");
    	await context.sync();

    	var data = new Object() ;
    	data["All"] = await getSheetData (context, sheets, "All");
    	
    	// if we have a sheet named "All" --> this is the data; otherwise collect all sheets per item type
    	if (data["All"] == null) {
    		
    		delete data["All"]; 
    		const sheetNames = ['MC', 'SC', 'FT', 'OR'];
    		for (var i = 0; i < sheetNames.length; i++) {
    			data[sheetNames[i]] = await getSheetData (context, sheets, sheetNames[i]);
    			if (data[sheetNames[i]] == null) {
    				delete data[sheetNames[i]];
    			}
    	    }
    	}	    	
    	fileData.val (JSON.stringify (data));
		downloadForm.submit();
		
	}); 
}




function sendPOST_Import (inputFormat, outputFormat, file, progress) {

	console.log("XXXassendPOST_Import");
	console.log(inputFormat);
	console.log(outputFormat);
	console.log(file.name);
	
	formData = new FormData();
	formData.append("file", file, file.name);
	formData.append("upload_file", true);
	formData.append("inputFormat", inputFormat);
	formData.append("outputFormat", outputFormat);

	$.ajax({
		url : "/convert",
		type : "POST",
		data : formData,
		contentType : false,
		cache : false,
		processData : false,
		success : function(data) {
			
			console.log("success");
			console.log (data);
		
			// /* progress max = number of excel rows to be inserted + 1 */
			// progress.max = 1;
			// for(var name in data) {
			// 	progress.max += data[name].length;
			// }
			// progress.value = 1;
			// progress.style.visibility = "Visible";

			// Excel.run(async function (context) { 
			// 	for(var name in data) {
			// 		await createSheet(context, progress, name, data[name]);
			// 	}
			// 	progress.style.visibility = "Hidden";
			// }); 
			
		},
		error : function(e) {
			console.log("error");
			console.log(e);
		}
	});
}




async function createSheet (context, progress, name, sheetData) {
	
	/* delete + create sheet */
	var sheet = context.workbook.worksheets.getItemOrNullObject(name);
	if (!(sheet == null)) {
		console.log ("Delete sheet " + name);
		sheet.delete();
	}
	sheet = context.workbook.worksheets.add(name);
	await context.sync();
	
	/* create header */
	const expensesTable = sheet.tables.add(sheet.getRangeByIndexes(0, 0, 1, sheetData[0].length), true);
	expensesTable.getHeaderRowRange().values = [sheetData[0]];
	progress.value += 1;
	await context.sync();
	
	/* add data by chunks */
	var rowCount = sheetData.length;
	var dataArr = [];
	var i;
	const chunkSize = 10;
	for (i=1; i<rowCount; i++) {
		dataArr.push (sheetData[i]);
		if (((i%chunkSize) == 0) || (i==(rowCount-1))) {
			progress.value += dataArr.length; 
			expensesTable.rows.add(null , dataArr);
			dataArr = [];
			await context.sync();
		}
	}	
	
	/* finishing */
	expensesTable.getDataBodyRange().format.autofitColumns();
	expensesTable.getDataBodyRange().format.autofitRows();
	sheet.visibility = "Visible";
	sheet.activate();
	await context.sync();
}





