
# Excel-Addin lokal nutzen

* Quelle: https://docs.microsoft.com/de-de/office/dev/add-ins/testing/create-a-network-shared-folder-catalog-for-task-pane-and-content-add-ins


\\DESKTOP-4E80VL2\excel
* Netzwerkfreigae auf Verzeichnis mit manifest.xml erstellen
* In Excel Einstellungen anpassen
    * Datei
    * Optionen
    * Trust Center
    * Einstellunge für Trustcenter
    * Kataloge vertrauenswürdiger Add-ins
    * URL angeben via Netzwerk-URL (z.B. \\MEINHOMEPC\Excel)
    * Katalog hinzufügen
    * Checkbox "In Menü anzeigen" aktivieren
* In Excel Add-in laden
    * Ribbon Einfügen
    * Meine Add-ins
    * Reiter Geteilter Ordner
    * Auswählen und Hinzufügen
* In Excel Add-in aktualisieren (wenn geändert)
    * Ribbon Einfügen
    * Meine Add-ins
    * Reiter Geteilter Ordner
    * rechts oben: Aktualisieren



# Fehler, wenn TaskPane localhost nicht aufrufen kann

* Quelle: https://docs.microsoft.com/en-us/office/troubleshoot/office-suite-issues/cannot-open-add-in-from-localhost
* als Admin in Powershell ausführen:

```powershell  
CheckNetIsolation LoopbackExempt -a -n="microsoft.win32webviewhost_cw5n1h2txyewy" 
``` 

* Zertifikat in Windows importieren: https://medium.com/@ali.dev/how-to-trust-any-self-signed-ssl-certificate-in-ie11-and-edge-fa7b416cac68




# Unsicheres https bei locahost

* in Chrome URL aufrufen und Ausnahme bestätigen
* dann sollte es in WebExcel öffnen (in Chrome)